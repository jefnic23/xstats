import pandas as pd
import numpy as np
from glob import glob
from scipy.spatial import KDTree
import pickle


def buildTree(tree, data, columns, file):
    d = {}
    for i, x in enumerate(tree.data):
        d.setdefault(i, [])
        
    for i, x in enumerate(data[:, :columns]):
        dist, point = tree.query(x, 1)
        try:
            d[point].append(data[i, columns])
        except KeyError:
            d[len(tree.data) - 1].append(data[i, columns])
        
    new_d = {}
    for k, v in d.items():
        try:
            new_d.setdefault(k, sum(filter(None, v)) / len(v))
        except:
            new_d.setdefault(k, 0)
        
    with open(file, 'wb') as f:
        pickle.dump(new_d, f)
        
def gatherData():
    strikeouts = ['strikeout', 'strikeout_double_play']
    savant = pd.concat([pd.read_csv(f, engine='python') for f in glob('D:/coding/baseball/baseball_savant/data/savant_*.csv')])  
    savant = savant[savant['game_year'] != 2022]
    savant['single'] = np.where(savant['events'] == 'single', 1, 0)
    savant['double'] = np.where(savant['events'] == 'double', 1, 0)
    savant['triple'] = np.where(savant['events'] == 'triple', 1, 0)
    savant['home_run'] = np.where(savant['events'] == 'home_run', 1, 0)
    savant['spray_angle'] = np.arctan((savant['hc_x'] - 125.42)/(198.27 - savant['hc_y'])) * 180 / np.pi * 0.75
    savant['spray_angle'] = np.where(savant['stand'] == 'L', 0 - savant['spray_angle'], savant['spray_angle'])
    savant['spray_angle'] = np.where(savant['events'].isin(strikeouts), 0, savant['spray_angle'])
    savant['hit_distance_sc'] = np.where(savant['events'].isin(strikeouts), 0, savant['hit_distance_sc'])
    savant['launch_angle'] = np.where(savant['events'].isin(strikeouts), 0, savant['launch_angle'])
    savant['launch_speed'] = np.where(savant['events'].isin(strikeouts), 0, savant['launch_speed'])
    return savant

# a = hit_distance
# x = spray_angle
# y = launch_angle
# z = launch_speed
xyz = [[a,x,y,z] for a in [0, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 17, 20, 25, 30, 38, 47, 58, 71, 86, 101, 117, 132, 147, 162, 177, 193, 210, 226, 241, 252, 263, 272, 280, 287, 294, 301, 308, 315, 322, 328, 336, 343, 351, 361, 372, 388, 495]
       for x in [-68, -34, -31, -27, -24, -22, -19, -16, -14, -11, -8, -5, -2, 1, 4, 7, 9, 12, 15, 18, 21, 24, 27, 31, 35, 67]
       for y in [-89, -44, -29, -21, -15, -11, -7, -3, 0, 3, 6, 9, 11, 14, 17, 20, 22, 25, 28, 31, 35, 40, 45, 53, 64, 90] 
       for z in [5, 54, 62, 66, 69, 72, 74, 76, 78, 79, 81, 82, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97, 98, 99, 100, 101, 102, 104, 106, 110]
       ]
    
print('building KDTree...')
kdtree = KDTree(xyz)
with open('lookup_tables/kdtree.pickle', 'wb') as f:
    pickle.dump(kdtree, f)
print('KDTree building complete\n')

print('gathering data...')
df = gatherData()
print('data gathering complete\n')

files = {'x1b': 93,
    'x2b': 94, 
    'x3b': 95,
    'xhr': 96
}

for k, v in files.items():
    print(f'building {k} lookup table...')
    m = df.iloc[:, [53, 97, 55, 54, v]].to_numpy()
    buildTree(kdtree, m, 4, f'lookup_tables/{k}.pickle')
    print(f'{k} lookup table complete\n')
print('all lookup tables built')
