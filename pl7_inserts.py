from xstat_functions import *
import pandas as pd
import pickle


with open('lookup_tables/kdtree.pickle', 'rb') as f:
    kdtree = pickle.load(f)
    
with open('lookup_tables/x1b.pickle', 'rb') as f:
    xsingle = pickle.load(f)
    
with open('lookup_tables/x2b.pickle', 'rb') as f:
    xdouble = pickle.load(f)
    
with open('lookup_tables/x3b.pickle', 'rb') as f:
    xtriple = pickle.load(f)
    
with open('lookup_tables/xhr.pickle', 'rb') as f:
    xhomerun = pickle.load(f)

df = pd.read_csv('testing/sportradar_data.csv').fillna(0)
df = df[df['year_played'] == 2021]
df['hbp'] = np.where(df['outcome_id'] == 'aHBP', 1, 0)
df['xsingle'] = df.apply(lambda row: xBASE(row['total_distance'], 
    row['coord_x'], 
    row['coord_y'], 
    row['hitterside'],
    row['launch_angle'], 
    row['launch_speed'], 
    kdtree, 
    xsingle
), axis=1)
df['xdouble'] = df.apply(lambda row: xBASE(row['total_distance'], 
    row['coord_x'], 
    row['coord_y'], 
    row['hitterside'],
    row['launch_angle'], 
    row['launch_speed'], 
    kdtree, 
    xdouble
), axis=1)
df['xtriple'] = df.apply(lambda row: xBASE(row['total_distance'], 
    row['coord_x'], 
    row['coord_y'], 
    row['hitterside'],
    row['launch_angle'], 
    row['launch_speed'], 
    kdtree, 
    xtriple
), axis=1)
df['xhomerun'] = df.apply(lambda row: xBASE(row['total_distance'], 
    row['coord_x'], 
    row['coord_y'], 
    row['hitterside'],
    row['launch_angle'], 
    row['launch_speed'], 
    kdtree, 
    xhomerun
), axis=1)

### Insert data ###

import os
from dotenv import load_dotenv
from sqlalchemy import create_engine


load_dotenv()
HOST = os.getenv('HOST')
NAME = os.getenv('NAME')
USER = os.getenv('USER')
PSWD = os.getenv('PSWD')

engine = create_engine(f'postgresql://{USER}:{PSWD}@{HOST}:5432/{NAME}')

for view in ['hitter', 'pitcher']:
    df_group = df.groupby([f'{view}_mlb_id', 'year_played']).agg({ \
        'xsingle': 'sum', \
        'xdouble': 'sum', \
        'xtriple': 'sum', \
        'xhomerun': 'sum', \
        'walk': 'sum', \
        'statcast_bbe': 'sum', \
        'hbp': 'sum', \
        'statcast_plate_appearance': 'sum', \
        'statcast_at_bat': 'sum' \
    })

    df_group['xhit'] = df_group['xsingle'] + df_group['xdouble'] + df_group['xtriple'] + df_group['xhomerun']
    df_group['x_avg'] = (df_group['xsingle'] + df_group['xdouble'] + df_group['xtriple'] + df_group['xhomerun']) / df_group['statcast_at_bat']
    df_group['x_slug_pct'] = (df_group['xsingle'] + df_group['xdouble']*2 + df_group['xtriple']*3 + df_group['xhomerun']*4) / df_group['statcast_at_bat']
    df_group['x_babip'] = (df_group['xsingle'] + df_group['xdouble'] + df_group['xtriple']) / (df_group['statcast_bbe'] - df_group['xhomerun'])
    df_group['x_woba'] = (df_group['walk']*0.692 + df_group['hbp']*0.722 + df_group['xsingle']*0.879 + df_group['xdouble']*1.242 + df_group['xtriple']*1.568 + df_group['xhomerun']*2.007) / df_group['statcast_plate_appearance']
    df_group['x_wobacon'] = (df_group['xsingle']*0.879 + df_group['xdouble']*1.242 + df_group['xtriple']*1.568 + df_group['xhomerun']*2.007) / df_group['statcast_bbe']
    df_group = df_group[['xhit', 'xsingle', 'xdouble', 'xtriple', 'x_avg', 'x_slug_pct', 'x_babip', 'x_woba', 'x_wobacon', 'xhomerun']].replace([np.inf, -np.inf], np.nan).fillna(0.0).rename_axis(['mlb_player_id', 'year_played'])
    
    df_group.to_sql(f'{view}_xstats_results', engine, if_exists='append')
