import numpy as np


def sprayAngle(coord_x, coord_y, hand):
    '''
    derives spray angle given x and y hit coordinates.
    if batter is left-handed, invert the spray angle.
    '''
    try:
        spray_angle = np.arctan((coord_x - 125.42)/(198.27 - coord_y)) * 180 / np.pi * 0.75
        if hand == "L":
            return 0 - spray_angle
        else:
            return spray_angle
    except ZeroDivisionError:
        return 0.0

def xBASE(hit_distance, coord_x, coord_y, hand, launch_angle, launch_speed, kdtree, base):
    '''
    derives expected single, double, triple, and homerun rates
    given hit distance, x and y hit coordinates, batter hand,
    launch angle and speed. base parameter is the lookup table 
    for whichever base hit type is being requested.
    '''
    spray_angle = sprayAngle(coord_x, coord_y, hand)
        
    # gets 3 nearest neighbors and their distances
    dist, points = kdtree.query([hit_distance, spray_angle, launch_angle, launch_speed], 3)
    
    # use the distances as weights
    try:
        point1, point2, point3 = base[points[0]] * dist[2], base[points[1]] * dist[1], base[points[2]] * dist[0]
        points, dist = point1 + point2 + point3, dist[0] + dist[1] + dist[2]
        return points / dist
    except:
        return 0.0
        
